package com.javahttp.server;

import com.javahttp.server.core.JavaServer;

import java.io.IOException;

public class JavaHttpServerApplication {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // running the Java server
        JavaServer.getInstance().start();
    }
}
