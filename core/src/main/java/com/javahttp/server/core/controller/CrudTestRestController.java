package com.javahttp.server.core.controller;

import com.javahttp.server.core.model.ControllerResponseData;
import com.javahttp.server.core.model.dto.CustomRequestBody;
import com.javahttp.server.core.type.HttpContentType;
import com.javahttp.server.core.type.HttpStatus;
import com.javahttp.server.core.type.meta.Controller;
import com.javahttp.server.core.type.meta.QueryVariable;
import com.javahttp.server.core.type.meta.RequestBody;
import com.javahttp.server.core.type.meta.request.Delete;
import com.javahttp.server.core.type.meta.request.Get;
import com.javahttp.server.core.type.meta.request.Post;
import com.javahttp.server.core.type.meta.request.Put;
import lombok.NonNull;

import java.util.*;

/**
 * Controller for testing CRUD (Create, Read, Update, Delete) standard operation responses
 */
@Controller("/crud")
public class CrudTestRestController {

    /**
     * Get All Entities operation
     *
     * @return collected entities
     */
    @Get(contentType = HttpContentType.APPLICATION_JSON)
    public Collection<CustomRequestBody> getAll() {
        Collection<CustomRequestBody> result = new ArrayList<>();
        result.add(CustomRequestBody.builder().id(1).value("Bob").arrayValues(List.of("Administrator", "Manager")).build());
        result.add(CustomRequestBody.builder().id(2).value("Stan").arrayValues(List.of("Manager")).build());
        result.add(CustomRequestBody.builder().id(3).value("Caroline").build());
        return result;
    }

    /**
     * Get Entity operation
     *
     * @param id the entity ID
     * @return {@link ControllerResponseData} collected data with a found entity if it exists
     */
    @Get(value = "/{id}", contentType = HttpContentType.APPLICATION_JSON)
    public ControllerResponseData httpGet(@QueryVariable("id") String id) {
        return ControllerResponseData.builder().httpStatus(HttpStatus.OK)
                .body(CustomRequestBody.builder().id(id != null ? Long.parseLong(id) : -1).value("Bob").arrayValues(List.of("Administrator", "Manager"))
                        .build()).build();
    }

    /**
     * Post operation
     *
     * @param body the entity to update
     * @return {@link ControllerResponseData} collected data with an updated entity if it was found
     */
    @Post(contentType = HttpContentType.APPLICATION_JSON)
    public ControllerResponseData httpPost(@NonNull @RequestBody CustomRequestBody body) {
        Map<String, String> headerValues = new HashMap<>();
        headerValues.put("Applied method", "Post");

        body.setValue("Modified value");

        return ControllerResponseData.builder().httpStatus(HttpStatus.OK).headerValues(headerValues).body(body).build();
    }

    /**
     * Put operation
     *
     * @param body the entity to update
     * @return {@link ControllerResponseData} collected data with partially updated entity if it was found
     */
    @Put(contentType = HttpContentType.APPLICATION_JSON)
    public ControllerResponseData httpPut(@NonNull @RequestBody CustomRequestBody body) {
        Map<String, String> headerValues = new HashMap<>();
        headerValues.put("Applied method", "Put");

        body.setValue("Modified value");

        Collection<String> modifiedValues;
        Collection<String> valuesArray = body.getArrayValues();
        if (valuesArray != null) {
            modifiedValues = new ArrayList<>();
            int iter = 0;
            for (String strValue : valuesArray) {
                if (iter > 0) {
                    modifiedValues.add(strValue);
                }
                iter++;
            }
        } else {
            modifiedValues = null;
        }
        body.setArrayValues(modifiedValues);

        return ControllerResponseData.builder().httpStatus(HttpStatus.OK).headerValues(headerValues).body(body).build();
    }

    /**
     * Delete operation
     *
     * @param key the entity ID
     * @return {@link ControllerResponseData} collected data with the result of deletion
     */
    @Delete("/{id}")
    public ControllerResponseData httpDelete(@QueryVariable("id") Long key) {
        Map<String, String> headerValues = new HashMap<>();
        headerValues.put("Received model ID", String.valueOf(key));
        if (key == null) {
            return ControllerResponseData.builder().httpStatus(HttpStatus.NOT_FOUND).body("Please specify the id to remove!").headerValues(headerValues).build();
        }
        return ControllerResponseData.builder().httpStatus(HttpStatus.OK).body("Done for id = " + key + "!").headerValues(headerValues).build();
    }
}
