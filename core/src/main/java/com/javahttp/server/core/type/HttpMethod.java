package com.javahttp.server.core.type;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

/**
 * Enum representing the common HTTP request methods
 */
@Getter
@RequiredArgsConstructor
public enum HttpMethod {
    GET("GET"),
    POST("POST"),
    PUT("PUT"),
    PATCH("PATCH"),
    DELETE("DELETE"),
    HEAD("HEAD"),
    OPTIONS("OPTIONS");

    private final String strValue;

    /**
     * Returns Http Method enum based on its string representation
     *
     * @param value the http method as String
     * @return http method as Enum (can be null)
     */
    public static Optional<HttpMethod> getMethodByValue(@NonNull String value) {
        return switch (value) {
            case "GET" -> Optional.of(HttpMethod.GET);
            case "POST" -> Optional.of(HttpMethod.POST);
            case "PUT" -> Optional.of(HttpMethod.PUT);
            case "PATCH" -> Optional.of(HttpMethod.PATCH);
            case "DELETE" -> Optional.of(HttpMethod.DELETE);
            case "HEAD" -> Optional.of(HttpMethod.HEAD);
            case "OPTIONS" -> Optional.of(HttpMethod.OPTIONS);
            default -> Optional.empty();
        };
    }
}
