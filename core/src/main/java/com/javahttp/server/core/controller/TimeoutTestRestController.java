package com.javahttp.server.core.controller;

import com.javahttp.server.core.model.ControllerResponseData;
import com.javahttp.server.core.type.HttpContentType;
import com.javahttp.server.core.type.HttpStatus;
import com.javahttp.server.core.type.meta.Controller;
import com.javahttp.server.core.type.meta.request.Delete;
import com.javahttp.server.core.type.meta.request.Get;
import com.javahttp.server.core.type.meta.request.Post;

/**
 * Controller for testing HTTP methods timeout
 */
@Controller(value = "/timeout", timeout = 5000L) // 10 seconds
public class TimeoutTestRestController {

    /**
     * Timeout by controller's value
     *
     * @return {string} a response message (should not be sent theoretically)
     */
    @Get(contentType = HttpContentType.UTF8)
    public String get() {
        waitExecution(8000); // 8 seconds
        return "Should not be sent";
    }

    /**
     * Timeout by endpoint's value
     *
     * @return {@link ControllerResponseData} collected data with the response result (actual data should not be sent theoretically)
     */
    @Post(contentType = HttpContentType.TEXT_PLAIN, timeout = 2000L) // 2 seconds
    public ControllerResponseData post() {
        waitExecution(3000); // 3 seconds
        return ControllerResponseData.builder().httpStatus(HttpStatus.OK).body("Should not be sent").build();
    }

    /**
     * Good request, timeout should not happen
     *
     * @return {@link ControllerResponseData} collected data with the response result
     */
    @Delete
    public ControllerResponseData delete() {
        return ControllerResponseData.builder().httpStatus(HttpStatus.OK).body("Good!").build();
    }

    /**
     * Delays the execution of the method thread for the specified time in milliseconds
     *
     * @param milliseconds time to wait (milliseconds)
     */
    private void waitExecution(long milliseconds) {
        try {
            Thread.sleep(milliseconds); // 8 seconds
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
