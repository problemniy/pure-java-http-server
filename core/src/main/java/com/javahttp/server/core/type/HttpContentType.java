package com.javahttp.server.core.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enum representing the regular HTTP content types
 */
@Getter
@RequiredArgsConstructor
public enum HttpContentType {
    TEXT_PLAIN("text/html"),
    UTF8("utf-8"),
    APPLICATION_JSON("application/json");

    private final String strValue;
}
