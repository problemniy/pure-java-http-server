package com.javahttp.server.core.model;

import com.javahttp.server.core.type.HttpStatus;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

/**
 * A class representing the controller's response data
 */
@Builder
@RequiredArgsConstructor
public class ControllerResponseData {

    private final Map<String, String> headerValues;

    private final HttpStatus httpStatus;

    private final Object body;

    public Map<String, String> getHeaderValues() {
        if (headerValues == null) {
            return Collections.emptyMap();
        }
        return headerValues;
    }

    public Optional<HttpStatus> getHttpStatus() {
        return Optional.ofNullable(httpStatus);
    }

    public Optional<Object> getBody() {
        return Optional.ofNullable(body);
    }
}
