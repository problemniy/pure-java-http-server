package com.javahttp.server.core.type.meta;

import com.javahttp.server.core.config.JavaServerConfigVariables;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation indicating that this class should be considered as a controller
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Controller {

    String value() default "";

    long timeout() default JavaServerConfigVariables.DEFAULT_REQUEST_TIMEOUT_MS;
}
