package com.javahttp.server.core.controller;

import com.javahttp.server.core.model.ControllerResponseData;
import com.javahttp.server.core.model.dto.CustomRequestBody;
import com.javahttp.server.core.type.HttpContentType;
import com.javahttp.server.core.type.HttpStatus;
import com.javahttp.server.core.type.meta.Controller;
import com.javahttp.server.core.type.meta.PathVariable;
import com.javahttp.server.core.type.meta.RequestBody;
import com.javahttp.server.core.type.meta.request.*;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller for testing existing HTTP methods
 */
@Controller("/method")
public class HttpMethodsTestRestController {

    /**
     * Get method call
     *
     * @return {@link ControllerResponseData} collected data with the response result and response message
     */
    @Get
    public ControllerResponseData httpGet(@PathVariable("key") String id) {
        return ControllerResponseData.builder().httpStatus(HttpStatus.OK).body("Hello 'Get' with param: key = " + id).build();
    }

    /**
     * Post method call
     *
     * @param body incoming body
     * @return {string} a response message
     */
    @Post(contentType = HttpContentType.UTF8)
    public String httpPost(@NonNull @RequestBody String body) {
        return "Hello 'Post' with body: " + body;
    }

    /**
     * Put method call
     *
     * @param body incoming body
     * @return {@link CustomRequestBody} the response entity
     */
    @Put(contentType = HttpContentType.APPLICATION_JSON)
    public CustomRequestBody httpPut(@NonNull @RequestBody CustomRequestBody body) {
        body.setValue("Hello 'Put'");
        Collection<String> valuesArray = body.getArrayValues();
        if (valuesArray == null) {
            valuesArray = new ArrayList<>();
        }
        valuesArray.add("Hello 'Put' from Array");
        return body;
    }

    /**
     * Patch method call
     *
     * @param body incoming entity
     * @return {@link ControllerResponseData} collected data with the response result and response entity
     */
    @Patch(contentType = HttpContentType.APPLICATION_JSON)
    public ControllerResponseData httpPatch(@RequestBody CustomRequestBody body) {
        if (body == null) {
            return ControllerResponseData.builder().httpStatus(HttpStatus.NO_CONTENT).build();
        }

        Collection<String> valuesArray = body.getArrayValues();
        if (valuesArray == null) {
            valuesArray = new ArrayList<>();
        }
        valuesArray.add("Hello 'Patch' from Array");
        return ControllerResponseData.builder().httpStatus(HttpStatus.OK).body(body).build();
    }

    /**
     * Delete method call
     *
     * @param key the entity ID
     * @return {string} a response message
     */
    @Delete
    public String httpDelete(@PathVariable("id") Long key) {
        return "Hello 'Delete' with param: id = " + key;
    }

    /**
     * Head method call
     *
     * @return {@link ControllerResponseData} collected data with the response result and modified headers
     */
    @Head(contentType = HttpContentType.TEXT_PLAIN)
    public ControllerResponseData httpHead() {
        Map<String, String> headerValues = new HashMap<>();
        headerValues.put("Notification", "Hello 'Head'");
        headerValues.put("Allow", "Get, Post, Put, Patch, Delete, Head");
        return ControllerResponseData.builder().httpStatus(HttpStatus.NO_CONTENT).headerValues(headerValues).build();
    }

    /**
     * Options method call
     *
     * @return {@link ControllerResponseData} collected data with the response result and modified headers
     */
    @Options
    public ControllerResponseData httpOptions() {
        Map<String, String> headerValues = new HashMap<>();
        headerValues.put("Notification", "Hello 'Options'");
        headerValues.put("Allow", "Get, Post, Put, Patch, Delete, Head");
        return ControllerResponseData.builder().httpStatus(HttpStatus.NO_CONTENT).headerValues(headerValues).build();
    }
}
