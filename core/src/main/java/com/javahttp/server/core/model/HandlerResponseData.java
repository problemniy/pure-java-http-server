package com.javahttp.server.core.model;

import com.javahttp.server.core.type.HttpContentType;
import com.javahttp.server.core.type.HttpStatus;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

/**
 * A class representing the handler's response data
 */
@Builder
@RequiredArgsConstructor
public class HandlerResponseData {

    private final HttpStatus httpStatus;

    private final Map<String, String> headers;

    private final String body;

    private final HttpContentType httpContentType;

    public Optional<HttpStatus> getHttpStatus() {
        return Optional.ofNullable(httpStatus);
    }

    public Map<String, String> getHeaders() {
        if (headers == null) {
            return Collections.emptyMap();
        }
        return headers;
    }

    public Optional<String> getBody() {
        return Optional.ofNullable(body);
    }

    public Optional<HttpContentType> getHttpContentType() {
        return Optional.ofNullable(httpContentType);
    }
}
