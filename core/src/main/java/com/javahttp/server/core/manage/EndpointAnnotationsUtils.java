package com.javahttp.server.core.manage;

import com.javahttp.server.core.config.ControllersScanPaths;
import com.javahttp.server.core.config.JavaServerConfigVariables;
import com.javahttp.server.core.model.EndpointMetaData;
import com.javahttp.server.core.type.HttpContentType;
import com.javahttp.server.core.type.HttpMethod;
import com.javahttp.server.core.type.meta.Controller;
import com.javahttp.server.core.type.meta.request.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Utils class that works with controller's HTTP annotations metadata
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EndpointAnnotationsUtils {

    /**
     * Returns the Content Type for the http method request if it was specified in the endpoint's annotation
     *
     * @param httpMethod http method request
     * @param endpoint   the endpoint for search
     * @return {@link HttpContentType} the Content Type (can be null)
     */
    public static Optional<HttpContentType> getHttpMethodContentType(HttpMethod httpMethod, Method endpoint) {
        var metaData = parseEndpointHttpMethodMeta(endpoint).orElse(null);
        return metaData != null && httpMethod == metaData.httpMethod() ? Optional.of(metaData.contentType()) : Optional.empty();
    }

    /**
     * Returns the endpoint's timeout if it was specified in the endpoint's annotation or the default one
     *
     * @param httpMethod http method request
     * @param endpoint   the endpoint for search
     * @return {long} the timeout
     */

    public static long getEndpointTimeout(HttpMethod httpMethod, Method endpoint) {
        long timeout = JavaServerConfigVariables.DEFAULT_REQUEST_TIMEOUT_MS;
        Controller controllerAnnotation = endpoint.getDeclaringClass().getAnnotation(Controller.class);
        if (controllerAnnotation != null) {
            timeout = controllerAnnotation.timeout();
        }
        var metaData = parseEndpointHttpMethodMeta(endpoint).orElse(null);
        return metaData != null && httpMethod == metaData.httpMethod() ? Math.min(metaData.timeout(), timeout) : timeout;
    }

    /**
     * Collects meta-information of controllers and created endpoints from the respective annotations;
     * References to controllers class paths must be explicitly specified in {@link ControllersScanPaths}
     *
     * @return {@link EndpointMetaData} the collected metadata
     */
    public static Collection<EndpointMetaData> collectRequestDelegatesData() throws ClassNotFoundException {
        Map<String, Collection<HttpMethod>> checkedPaths = new HashMap<>();
        Map<Class<?>, String> classEndpointPaths = new HashMap<>();

        Map<Class<?>, Map<HttpMethod, Map<String, Method>>> tempDelegatesData = new HashMap<>();
        Collection<EndpointMetaData> requestsData = new ArrayList<>();

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        for (String classPath : ControllersScanPaths.CLASS_REFERENCES) {
            Class<?> loadedClass = classLoader.loadClass(classPath);
            if (loadedClass != null) {
                Controller controllerAnnotation = loadedClass.getAnnotation(Controller.class);
                if (controllerAnnotation != null) {
                    String controllerPath = controllerAnnotation.value();
                    classEndpointPaths.put(loadedClass, controllerPath.startsWith("/") ? controllerPath : "/" + controllerPath);

                    for (Method method : loadedClass.getMethods()) {
                        var metaData = parseEndpointHttpMethodMeta(method).orElse(null);
                        if (metaData != null) {
                            StringBuilder path = new StringBuilder(controllerPath);
                            HttpMethod httpMethod = metaData.httpMethod();
                            String methodPath = metaData.path();
                            appendPath(path, methodPath);

                            String actualPath = getActualPath(path.toString());
                            Collection<HttpMethod> methods = checkedPaths.get(actualPath);
                            if (methods != null && methods.contains(httpMethod)) {
                                throw new IllegalStateException("multiple controllers register the same path: '" + path + "'");
                            }
                            checkedPaths.computeIfAbsent(actualPath, k -> new ArrayList<>()).add(httpMethod);

                            tempDelegatesData.computeIfAbsent(loadedClass, k -> new HashMap<>())
                                    .computeIfAbsent(httpMethod, k -> new HashMap<>())
                                    .put(actualPath, method);
                        }
                    }
                }
            }
        }

        tempDelegatesData.forEach((clazz, httpMethodPathEndpoints)
                -> requestsData.add(new EndpointMetaData(clazz, classEndpointPaths.get(clazz), httpMethodPathEndpoints)));

        return requestsData;
    }

    /**
     * Returns the path without an extra slash at the end
     *
     * @return {string} the converted path
     */
    public static String getActualPath(String rawPath) {
        StringBuilder pathBuilder = new StringBuilder();
        for (String pathPart : rawPath.split("/")) {
            if (!pathPart.isBlank()) {
                pathBuilder.append("/").append(pathPart);
            }
        }
        return pathBuilder.toString();
    }

    /**
     * Appends the provided URI to the final URL path
     *
     * @param path     the result URL path
     * @param uriValue the URI to append
     */
    private static void appendPath(StringBuilder path, @NonNull String uriValue) {
        if (!uriValue.isBlank() && !uriValue.equals("/")) {
            if (!uriValue.startsWith("/")) {
                path.append("/");
            }
            path.append(uriValue);
        }
    }

    /**
     * Represents the http method's annotation data
     *
     * @param httpMethod  specified the http method's
     * @param path        specified the path
     * @param contentType specified the content type
     * @param timeout     specified timeout
     */
    private record AnnotationHttpMethodMetaData(
            HttpMethod httpMethod,
            String path,
            HttpContentType contentType,
            long timeout) {
    }

    /**
     * Returns endpoint metadata from the specified HTTP method's annotation if present above the method
     *
     * @param endpoint the endpoint to check
     * @return {@link AnnotationHttpMethodMetaData} the collected annotation's metadata (can be null)
     */
    private static Optional<AnnotationHttpMethodMetaData> parseEndpointHttpMethodMeta(Method endpoint) {
        for (Annotation methodAnnotation : endpoint.getAnnotations()) {
            if (methodAnnotation instanceof Get annotation) {
                return Optional.of(new AnnotationHttpMethodMetaData(
                        HttpMethod.GET, annotation.value(),
                        annotation.contentType(),
                        annotation.timeout()));
            } else if (methodAnnotation instanceof Post annotation) {
                return Optional.of(new AnnotationHttpMethodMetaData(
                        HttpMethod.POST, annotation.value(),
                        annotation.contentType(),
                        annotation.timeout()));
            } else if (methodAnnotation instanceof Put annotation) {
                return Optional.of(new AnnotationHttpMethodMetaData(
                        HttpMethod.PUT, annotation.value(),
                        annotation.contentType(),
                        annotation.timeout()));
            } else if (methodAnnotation instanceof Patch annotation) {
                return Optional.of(new AnnotationHttpMethodMetaData(
                        HttpMethod.PATCH, annotation.value(),
                        annotation.contentType(),
                        annotation.timeout()));
            } else if (methodAnnotation instanceof Delete annotation) {
                return Optional.of(new AnnotationHttpMethodMetaData(
                        HttpMethod.DELETE, annotation.value(),
                        annotation.contentType(),
                        annotation.timeout()));
            } else if (methodAnnotation instanceof Head annotation) {
                return Optional.of(new AnnotationHttpMethodMetaData(
                        HttpMethod.HEAD, annotation.value(),
                        annotation.contentType(),
                        annotation.timeout()));
            } else if (methodAnnotation instanceof Options annotation) {
                return Optional.of(new AnnotationHttpMethodMetaData(
                        HttpMethod.OPTIONS, annotation.value(),
                        annotation.contentType(),
                        annotation.timeout()));
            }
        }
        return Optional.empty();
    }
}
