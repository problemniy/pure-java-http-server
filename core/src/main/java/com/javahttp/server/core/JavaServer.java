package com.javahttp.server.core;

import com.javahttp.server.core.config.JavaServerConfigVariables;
import com.javahttp.server.core.handler.RequestHandler;
import com.javahttp.server.core.manage.EndpointAnnotationsUtils;
import com.sun.net.httpserver.HttpServer;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Class that launches and manages the Java server
 */
@Getter
public final class JavaServer {

    @Getter(value = AccessLevel.NONE)
    private static volatile JavaServer instance;
    @Getter(value = AccessLevel.NONE)
    private static final Object mutex = new Object();

    private final HttpServer serverInstance;
    private final Executor connectionsPool;

    /**
     * Returns the created Java server's instance
     *
     * @return created server's instance
     */
    public static JavaServer getInstance() throws IOException, ClassNotFoundException {
        JavaServer result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null)
                    instance = result = new JavaServer();
            }
        }
        return result;
    }

    /**
     * Creates an instance of the server based on a shared configuration
     *
     * @see JavaServerConfigVariables
     * @see com.javahttp.server.core.config.ControllersScanPaths
     */
    private JavaServer() throws IOException, ClassNotFoundException {
        connectionsPool = Executors.newFixedThreadPool(JavaServerConfigVariables.MAX_CONNECTIONS_POOL_NUMBER);
        serverInstance = HttpServer.create(new InetSocketAddress(JavaServerConfigVariables.SERVER_ADDRESS, JavaServerConfigVariables.SERVER_PORT), 0);
        serverInstance.setExecutor(connectionsPool);
        addControllerHandlers(serverInstance);
    }

    /**
     * Processes metadata of the found controller classes and adds corresponding handlers for incoming HTTP requests
     *
     * @param serverInstance a created Java server's instance
     */
    private void addControllerHandlers(HttpServer serverInstance) throws ClassNotFoundException {
        EndpointAnnotationsUtils.collectRequestDelegatesData()
                .forEach(requestData -> {
                    try {
                        serverInstance.createContext(requestData.mainEndpointPath(),
                                new RequestHandler(requestData.clazz().getDeclaredConstructor().newInstance(), requestData.httpMethodPathEndpoints()));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    /**
     * Launches the Java server
     */
    public void start() {
        serverInstance.start();
    }
}
