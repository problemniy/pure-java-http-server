package com.javahttp.server.core.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Configuration class for server settings
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JavaServerConfigVariables {

    public static final String SERVER_ADDRESS = "localhost";
    public static final int SERVER_PORT = 8080;
    public static final int MAX_CONNECTIONS_POOL_NUMBER = 100;

    // milliseconds
    public static final long DEFAULT_REQUEST_TIMEOUT_MS = 180000L;
}
