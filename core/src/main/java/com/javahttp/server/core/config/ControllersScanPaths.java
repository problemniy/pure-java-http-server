package com.javahttp.server.core.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;

/**
 * Configuration class specifying the references to classes that should be considered as controllers for handling incoming HTTP requests
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ControllersScanPaths {

    public static final Collection<String> CLASS_REFERENCES = List.of(
            "com.javahttp.server.core.controller.CrudTestRestController",
            "com.javahttp.server.core.controller.HttpMethodsTestRestController",
            "com.javahttp.server.core.controller.TimeoutTestRestController"
    );
}
