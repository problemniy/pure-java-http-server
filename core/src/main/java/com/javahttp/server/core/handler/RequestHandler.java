package com.javahttp.server.core.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javahttp.server.core.manage.EndpointAnnotationsUtils;
import com.javahttp.server.core.model.ControllerResponseData;
import com.javahttp.server.core.model.HandlerResponseData;
import com.javahttp.server.core.type.HttpContentType;
import com.javahttp.server.core.type.HttpMethod;
import com.javahttp.server.core.type.HttpStatus;
import com.javahttp.server.core.type.meta.PathVariable;
import com.javahttp.server.core.type.meta.QueryVariable;
import com.javahttp.server.core.type.meta.RequestBody;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The main handler that processes all incoming HTTP requests
 */
@RequiredArgsConstructor
public class RequestHandler implements HttpHandler {

    private static final ObjectMapper jsonMapper = new ObjectMapper();

    private final Object controller;
    private final Map<HttpMethod, Map<String, Method>> httpMethodPathEndpoints;

    /**
     * The main handler's endpoint of incoming HTTP request to generate an appropriate response
     *
     * @param exchange the exchange containing the request from the client and used to send the response
     */
    @Override
    public void handle(HttpExchange exchange) {
        try (InputStream inputBodyStream = exchange.getRequestBody(); OutputStream outputBodyStream = exchange.getResponseBody()) {
            HandlerResponseData handlerResponseData;
            HttpMethod httpMethod = HttpMethod.getMethodByValue(exchange.getRequestMethod()).orElse(null);
            if (httpMethod == null) {
                handlerResponseData = proceedUndefinedEndpoint();
            } else {
                Map<String, String> requestParams = getRequestParams(exchange);
                String requestBody = getBodyString(inputBodyStream).orElse("");
                handlerResponseData = handleSuitableMethods(httpMethod, exchange.getRequestURI(), requestParams, requestBody);
            }

            proceedResponseData(exchange, outputBodyStream, httpMethod, handlerResponseData);
        } catch (Exception exc) {
            throw new RuntimeException(exc);
        }
    }

    /**
     * Returns the HTTP request parameters
     *
     * @param exchange http exchange
     * @return {map} the request parameters with values
     */
    private Map<String, String> getRequestParams(HttpExchange exchange) {
        Map<String, String> requestParams = new HashMap<>();

        String paramsQuery = exchange.getRequestURI().getQuery();
        if (paramsQuery != null && !paramsQuery.isBlank()) {
            String[] splitParams = paramsQuery.split("&");
            for (String paramData : splitParams) {
                String[] keyVal = paramData.split("=");
                requestParams.put(keyVal[0], keyVal.length == 1 ? "" : keyVal[1]);
            }
        }

        return requestParams;
    }

    /**
     * Returns the incoming body as String
     *
     * @param inputBodyStream incoming body stream
     * @return {string} the incoming body (can be null)
     */
    private Optional<String> getBodyString(InputStream inputBodyStream) throws IOException {
        if (inputBodyStream.available() <= 0) {
            return Optional.empty();
        }
        String bodyStr = new String(inputBodyStream.readAllBytes(), StandardCharsets.UTF_8);
        return Optional.of(bodyStr);
    }

    /**
     * Checks the endpoint for specified query parameters and collects corresponding values if matches are found in the incoming request
     *
     * @param uriPath       the URI path
     * @param pathEndpoints contains paths with their corresponding endpoints
     * @param queryParams   query parameters with values
     * @return {map} the query parameters with values
     */
    private Map<String, Method> withQueryParamsInvestigation(String uriPath, Map<String, Method> pathEndpoints, Map<String, String> queryParams) {
        Map<String, Method> finalEndpoints = new HashMap<>();
        pathEndpoints.forEach((path, endpoints) -> {
            Matcher match = Pattern.compile("\\{(.+?)}").matcher(path);
            if (match.find()) {
                String leftRawPath = path.substring(0, match.start());
                String leftPart = EndpointAnnotationsUtils.getActualPath(leftRawPath);
                if (uriPath.startsWith(leftPart) && !uriPath.equals(leftPart)) {
                    String key = match.group().split("([{}])")[1];
                    String val = uriPath.replace(leftRawPath, "");
                    if (!val.isBlank()) {
                        queryParams.put(key, uriPath.replace(leftRawPath, ""));
                    }
                    String updatedPath = EndpointAnnotationsUtils.getActualPath(uriPath);
                    finalEndpoints.put(updatedPath, endpoints);
                }
            } else {
                finalEndpoints.put(path, endpoints);
            }
        });
        return finalEndpoints;
    }

    /**
     * Processes the corresponding method if found based on the incoming request and collects response data
     *
     * @param httpMethod    incoming http method
     * @param uri           incoming URI
     * @param requestParams request parameters with values
     * @param requestBody   incoming request body
     * @return {@link HandlerResponseData} collected response data
     */
    private HandlerResponseData handleSuitableMethods(HttpMethod httpMethod, URI uri, Map<String, String> requestParams, String requestBody)
            throws ExecutionException, InterruptedException, JsonProcessingException {
        Map<String, Method> pathEndpoints = httpMethodPathEndpoints.get(httpMethod);
        if (pathEndpoints == null) {
            return proceedUndefinedEndpoint();
        }

        Map<String, String> queryParams = new HashMap<>();
        String uriPath = uri.getPath();
        pathEndpoints = withQueryParamsInvestigation(uriPath, pathEndpoints, queryParams);

        Method endpoint = pathEndpoints.get(EndpointAnnotationsUtils.getActualPath(uriPath));
        if (endpoint == null) {
            return proceedUndefinedEndpoint();
        }

        Object responseEntity;
        Object[] paramValues = getEndpointParamValues(endpoint, queryParams, requestParams, requestBody);

        try {
            long timeout = EndpointAnnotationsUtils.getEndpointTimeout(httpMethod, endpoint);
            responseEntity = executeMethodWithTimeout(endpoint, controller, paramValues, timeout);
        } catch (TimeoutException e) {
            return proceedDefaultEmptyEndpoint(HttpStatus.REQUEST_TIMEOUT);
        }

        return parseResponseEntity(EndpointAnnotationsUtils.getHttpMethodContentType(httpMethod, endpoint).orElse(null), responseEntity);
    }

    /**
     * Collects parameter values that need to be sent to the controller method based on its metadata
     *
     * @param endpoint      controller's endpoint
     * @param queryParams   query parameters with values
     * @param requestParams request parameters with values
     * @param requestBody   incoming request body
     * @return {array} parameter values
     */
    private Object[] getEndpointParamValues(Method endpoint, Map<String, String> queryParams, Map<String, String> requestParams, String requestBody) {
        Parameter[] parameters = endpoint.getParameters();
        Object[] paramValues = new Object[parameters.length];

        for (int i = 0; i < parameters.length; i++) {
            QueryVariable queryVariable = parameters[i].getAnnotation(QueryVariable.class);
            if (queryVariable != null) {
                String queryKey = queryVariable.value();
                String keyValue = queryParams.get(queryKey);
                paramValues[i] = applyPrimitiveParameter(keyValue, parameters[i]);
                continue;
            }

            PathVariable pathVariable = parameters[i].getAnnotation(PathVariable.class);
            if (pathVariable != null) {
                String paramValue = requestParams.get(pathVariable.value());
                paramValues[i] = applyPrimitiveParameter(paramValue, parameters[i]);
                continue;
            }

            if (!requestBody.isBlank()) {
                RequestBody bodyParam = parameters[i].getAnnotation(RequestBody.class);
                if (bodyParam != null) {
                    if (parameters[i].getType().equals(String.class)) {
                        paramValues[i] = requestBody;
                    } else try {
                        paramValues[i] = jsonMapper.readValue(requestBody, parameters[i].getType());
                    } catch (JsonProcessingException e) {
                        paramValues[i] = null;
                    }
                    continue;
                }
            }

            paramValues[i] = null;
        }

        return paramValues;
    }

    /**
     * Converts the value of the specified primitive parameter if necessary
     *
     * @param paramValue the parameter's value
     * @param parameter  the endpoint's parameter
     * @return {object} converted parameter's value
     */
    private Object applyPrimitiveParameter(String paramValue, Parameter parameter) {
        try {
            if (parameter.getType().equals(boolean.class) || parameter.getType().equals(Boolean.class)) {
                return Boolean.valueOf(paramValue);
            } else if (parameter.getType().equals(byte.class) || parameter.getType().equals(Byte.class)) {
                return Byte.valueOf(paramValue);
            } else if (parameter.getType().equals(short.class) || parameter.getType().equals(Short.class)) {
                return Short.valueOf(paramValue);
            } else if (parameter.getType().equals(int.class) || parameter.getType().equals(Integer.class)) {
                return Integer.valueOf(paramValue);
            } else if (parameter.getType().equals(long.class) || parameter.getType().equals(Long.class)) {
                return Long.valueOf(paramValue);
            } else if (parameter.getType().equals(float.class) || parameter.getType().equals(Float.class)) {
                return Float.valueOf(paramValue);
            } else if (parameter.getType().equals(double.class) || parameter.getType().equals(Double.class)) {
                return Double.valueOf(paramValue);
            }
        } catch (Exception exc) {
            return paramValue;
        }
        return paramValue;
    }

    /**
     * Processes the entity returned by the controller's endpoint and returns the collected response data
     *
     * @param contentType    the content type to apply
     * @param responseEntity the controller's response entity
     * @return {@link HandlerResponseData} collected response data
     */
    private HandlerResponseData parseResponseEntity(HttpContentType contentType, Object responseEntity) throws JsonProcessingException {
        if (responseEntity == null) {
            return proceedDefaultEmptyEndpoint(HttpStatus.NOT_FOUND);
        }

        String body = getBodyAsString(responseEntity);
        if (responseEntity instanceof ControllerResponseData controllerData) {
            return HandlerResponseData.builder()
                    .httpStatus(controllerData.getHttpStatus().orElse(HttpStatus.OK))
                    .headers(controllerData.getHeaderValues())
                    .httpContentType(contentType)
                    .body(body)
                    .build();
        } else {
            return HandlerResponseData.builder()
                    .httpStatus(HttpStatus.OK)
                    .httpContentType(contentType)
                    .body(body)
                    .build();
        }
    }

    /**
     * Converts the given object into a String representation
     *
     * @param responseEntity the controller's response entity
     * @return {string} converted object
     */
    private String getBodyAsString(Object responseEntity) throws JsonProcessingException {
        String bodyObjectStr;
        if (responseEntity instanceof ControllerResponseData controllerData) {
            Object bodyObject = controllerData.getBody().orElse("");
            if (bodyObject instanceof String) {
                bodyObjectStr = (String) bodyObject;
            } else {
                bodyObjectStr = jsonMapper.writeValueAsString(bodyObject);
            }
        } else {
            if (responseEntity instanceof String) {
                bodyObjectStr = (String) responseEntity;
            } else {
                bodyObjectStr = jsonMapper.writeValueAsString(responseEntity);
            }
        }
        return bodyObjectStr;
    }

    /**
     * Returns response data for requests that have not been processed by any controller's endpoint
     *
     * @return {@link HandlerResponseData} collected response data
     */
    private HandlerResponseData proceedUndefinedEndpoint() {
        return HandlerResponseData.builder().httpStatus(HttpStatus.NOT_FOUND).httpContentType(HttpContentType.TEXT_PLAIN).build();
    }

    /**
     * Returns response data for requests if the endpoint processed the request but the response is empty
     *
     * @param httpStatus the response http status
     * @return {@link HandlerResponseData} collected response data
     */
    private HandlerResponseData proceedDefaultEmptyEndpoint(HttpStatus httpStatus) {
        if (HttpStatus.REQUEST_TIMEOUT == httpStatus) {
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-type", "text/html; charset=" + HttpContentType.UTF8.getStrValue());
            String bodyDefaultMessage = "408 Request Timeout";
            return HandlerResponseData.builder().httpStatus(HttpStatus.REQUEST_TIMEOUT).headers(headers).body(bodyDefaultMessage).httpContentType(HttpContentType.TEXT_PLAIN).build();
        } else {
            return HandlerResponseData.builder().httpStatus(httpStatus == null ? HttpStatus.OK : httpStatus).httpContentType(HttpContentType.TEXT_PLAIN).build();
        }
    }

    /**
     * Processes the collected response data and sends it as a response to the incoming HTTP request
     *
     * @param exchange            http exchange
     * @param outputBodyStream    the response body as stream
     * @param httpMethod          incoming http method
     * @param handlerResponseData collected response data
     */
    private void proceedResponseData(HttpExchange exchange, OutputStream outputBodyStream, HttpMethod httpMethod, HandlerResponseData handlerResponseData) throws IOException {
        HttpStatus httpStatus = handlerResponseData.getHttpStatus().orElse(HttpStatus.OK);
        Map<String, String> headers = handlerResponseData.getHeaders();
        HttpContentType httpContentType = handlerResponseData.getHttpContentType().orElse(null);
        String body = handlerResponseData.getBody().orElse("");

        Headers exchangeHeaders = exchange.getResponseHeaders();
        headers.forEach(exchangeHeaders::set);

        if (httpContentType != null) {
            if (httpContentType == HttpContentType.UTF8) {
                exchangeHeaders.set("Content-type", "text/html; charset=" + HttpContentType.UTF8.getStrValue());
            } else {
                exchangeHeaders.set("Content-type", httpContentType.getStrValue());
            }
        } else {
            httpContentType = HttpContentType.TEXT_PLAIN;
            if (!exchangeHeaders.containsKey("Content-type")) {
                exchangeHeaders.set("Content-type", HttpContentType.TEXT_PLAIN.getStrValue());
            }
        }

        if (HttpStatus.NO_CONTENT == httpStatus || HttpMethod.HEAD == httpMethod || HttpMethod.OPTIONS == httpMethod) {
            exchange.sendResponseHeaders(httpStatus.getValue(), 0);
        } else {
            exchange.sendResponseHeaders(httpStatus.getValue(), body.length());
            if (HttpContentType.UTF8 == httpContentType || HttpContentType.APPLICATION_JSON == httpContentType) {
                try (Writer responseWriter = new OutputStreamWriter(outputBodyStream, StandardCharsets.UTF_8)) {
                    responseWriter.write(body);
                }
            } else {
                outputBodyStream.write(body.getBytes());
            }
        }
    }

    /**
     * Wraps the invoked method of the controller's endpoint to set and monitor the specified timeout and interrupt execution if necessary
     *
     * @param method  controller's endpoint
     * @param object  controller's object
     * @param params  the endpoint's parameter values to send
     * @param timeout endpoint's execution timeout (milliseconds)
     * @return {object} the endpoint's result object
     * @throws TimeoutException if the timeout has elapsed but the method is still executing
     */
    private Object executeMethodWithTimeout(Method method, Object object, Object[] params, long timeout)
            throws ExecutionException, InterruptedException, TimeoutException {
        try (var executor = Executors.newSingleThreadExecutor()) {
            Future<?> threadFuture = executor.submit(() -> {
                try {
                    return method.invoke(object, params);
                } catch (Exception exc) {
                    throw new RuntimeException(exc);
                }
            });
            return threadFuture.get(timeout, TimeUnit.MILLISECONDS);
        }
    }
}
