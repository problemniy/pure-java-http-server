package com.javahttp.server.core.type.meta.request;

import com.javahttp.server.core.type.HttpContentType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.javahttp.server.core.config.JavaServerConfigVariables.DEFAULT_REQUEST_TIMEOUT_MS;

/**
 * Annotation indicating that this method should handle an Options HTTP request
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Options {

    String value() default "";

    HttpContentType contentType() default HttpContentType.TEXT_PLAIN;

    long timeout() default DEFAULT_REQUEST_TIMEOUT_MS;
}
