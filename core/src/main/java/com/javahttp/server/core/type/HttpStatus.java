package com.javahttp.server.core.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enum representing the regular HTTP response statuses
 */
@Getter
@RequiredArgsConstructor
public enum HttpStatus {

    OK(200, "OK"),
    BAD_REQUEST(400, "Bad Request"),
    CREATE(201, "Created"),
    NO_CONTENT(204, "No Content"),
    NOT_FOUND(404, "Not Found"),
    REQUEST_TIMEOUT(408, "Request Timeout");

    private final int value;
    private final String reasonPhrase;
}
