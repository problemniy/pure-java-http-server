package com.javahttp.server.core.model;

import com.javahttp.server.core.type.HttpMethod;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * A record class representing the endpoint's annotations metadata
 */
public record EndpointMetaData(
        Class<?> clazz,
        String mainEndpointPath,
        Map<HttpMethod, Map<String, Method>> httpMethodPathEndpoints
) {

}
