package com.javahttp.server.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * A class representing the regular DTO (Data Transfer Object)
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomRequestBody {

    private long id;

    private String value;

    private Collection<String> arrayValues;
}
