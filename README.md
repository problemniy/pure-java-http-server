## Pure Java HTTP Server

![Project Status: Active](https://img.shields.io/badge/Project_Status-Active-darkgreen.svg)
![Java](https://img.shields.io/badge/Java-%23ED8B00.svg?logo=openjdk&logoColor=white)
![100% Java](https://img.shields.io/badge/java-green?label=100%25&color=red)

A server-side application written in pure Java that handles incoming HTTP requests of various types

- Processes standard HTTP requests (*GET, POST, PUT, PATCH, HEAD, OPTIONS, DELETE*)
- Routes incoming requests to the corresponding controller classes
- Provides annotations for creating custom controller classes and extracting incoming data
- Transmitted content types support: *text/html, utf-8, application/json*

## Hello Worlds!

To deploy the application, you need to:

- Use Java 21 version
- Use Postman

Provided annotations:

```
@Controller       - indicates that this class is a controller
@PathVariable     - indicates that this method argument is a request parameter
@QueryVariable    - indicates that this method argument is part of the URI request
@RequestBody      - indicates that this argument is the request body
```

Auxiliary Enum classes:

```
HttpContentType   - specifies the transmitted content type
HttpMethod        - specifies the type of HTTP request
HttpStatus        - specifies the type of response status
```

### Example of creating and using a simple controller:

```java

@Controller("/rest")
public class MyRestController {
    @Put(contentType = HttpContentType.APPLICATION_JSON)
    public CustomRequestBody httpPut(@NonNull @RequestBody CustomRequestBody body) {
        // some logic
        return ControllerResponseData.builder()
                .httpStatus(HttpStatus.OK)
                .headerValues(headerValues)
                .body(body).build();
    }
}
```

You can view examples of already created controllers that are used for testing via Postman:
<br>[CrudTestRestController.java](core/src/main/java/com/javahttp/server/core/controller/CrudTestRestController.java)
<br>[HttpMethodsTestRestController.java](core/src/main/java/com/javahttp/server/core/controller/HttpMethodsTestRestController.java)
<br>[TimeoutTestRestController.java](core/src/main/java/com/javahttp/server/core/controller/TimeoutTestRestController.java)

Returning content from the controller endpoint can be done in two ways:

- Native, simply by returning an object that will be serialized into the body.
- Declarative, using an object of
  type [ControllerResponseData](core/src/main/java/com/javahttp/server/core/model/ControllerResponseData.java)
  *<br>(allows for more flexible configuration of the returned response by setting headers, body, and response status)*

You can use a builder that immediately allows you to create a ControllerResponseData

```java

@Delete
public ControllerResponseData delete() {
    return ControllerResponseData.builder().httpStatus(HttpStatus.OK).body("Good!").build();
}
```

## Building from Source

[Gradle](https://www.gradle.org) is used to build, test, and publish. JDK 1.21 or higher is required.
<br>To build the project, run:

    ./gradlew clean build

- Upload the [postman-collection](src/main/resources/json.postman/pure_java_http_server.postman_collection.json)
  script to the Postman
  <br>*you can simply use drag & drop*

## Notes

To test receiving and responding to HTTP requests, you can use [Postman](https://www.postman.com) by uploading a
prepared collection from the resource package beforehand
<br><br>This will create a group of Java HTTP Server requests in Postman for three existing controllers

```
Java HTTP Server
  - Method
  - Crud
  - Timeout
```

, after which you will be able to invoke them

## Author

@problemniy
